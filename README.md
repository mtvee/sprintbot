SprintBot
=========

A quickly hacked up irc bot for word sprints

Usage:
------
`$> scriptname --help`

IRC Usage:
----------
* Anyone can start a sprint with `!sprint 5 10`.
  This will queue a sprint to start in 5 minutes and lasting
  for 10 minutes. The user that started the sprint is automatically
  joined.

* User can join a sprint with `!join`

* User can withdraw from a sprint with `!withdraw`. If everyone withdraws the
  sprint is aborted.

* The user that started the sprint or the script owner can abort
  a sprint with `!abort`

* User can set thier base count with `!wc set #`

* User can reset their base count with `!wc reset`

* User can set thier current count with `!wc #`. If a base count is set, it is
  subtracted from this number.

* When the sprint ends the user with the highest `!wc #` wins.

* Anyone can get the current state of a sprint with `!status`, `!time`
  or `!sprint`

* You can have the bot notify when a sprint is announced with `!notify` or
  `!pingme` and it will ping you in the channel. The list is erased after the
  sprint is announced.

Requires:
---------
python 2.7 and twisted
   
`$> pip install twisted`

__NOTE:__ at this point, twisted is no go in python 3, ugh!
should've built this mess to asynccore


