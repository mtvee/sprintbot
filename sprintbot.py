# -*- coding: utf-8 -*-
"""
A quickly hacked up irc bot for word sprints

Usage:
------
`$> scriptname --help`

IRC Usage:
----------
* Anyone can start a sprint with `!sprint 5 10`.
  This will queue a sprint to start in 5 minutes and lasting
  for 10 minutes. The user that started the sprint is automatically
  joined.

* User can join a sprint with `!join`. Optionally you can add your base
  wordcount e.g. `!join 1234` 

* User can withdraw from a sprint with `!withdraw`. If everyone withdraws the
  sprint is aborted.

* The user that started the sprint or the script owner can abort
  a sprint with `!abort`

* User can set their base count with `!wc set #`

* User can reset their base count with `!wc reset`

* User can set their current count with `!wc #`. If a base count is set it is
  subtracted from this number.

* User can see their counts with `!wc` while a sprint is running.

* When the sprint ends the user with the highest `!wc #` wins.

* Anyone can get the current state of a sprint with `!status`, `!time`
  or `!sprint`

* When sprint ends, waits for users to update their word counts. Once everyone
  that was in the sprint updates, the sprint ends, or times out and ends
  if someone didn't update.

* While the sprint is waiting, the person that started the sprint can
  `!finalize` to force a winner and end the sprint. This is in case someone
  in the sprint left or lost connection so as not to have to wait for
  the sprint to time out.

* You can have the bot notify when a sprint is announced with `!notify` or
  `!pingme` and it will ping you in the channel. The list is erased after the
  sprint is announced.

Requires:
---------
python 2.7 and twisted
   $> pip install twisted

NOTE: at this point, twisted is no go in python 3, ugh
      need to move this mess to asynccore
"""

from twisted.words.protocols import irc
from twisted.internet import protocol, reactor, task
import time
import argparse
import random
import logging
from datetime import date

# -----------------------------------------------
# you can hit all these from the command line too
# -----------------------------------------------
# the bot nickname
NICK = 'sprintbot'
# the channel to join
CHANNEL = 'r-nanowrimo'
# the irc server
SERVER = 'irc.freenode.net'
# the server port
SERVER_PORT = 6667
# who owns this bot (so you can sort of control it, put in your nick)
# this only comes into play in the !abort command and you can make
# the bot say stuff
OWNER_NICK = 'mtvee'
# -----------------------------------------------

class SprintBot(irc.IRCClient):
    """A basic sprintbot/jotbot to manage writing sprints on IRC
    """
    # states of being (i think there is an enum class somewhere)
    STATE_NONE = 0  # nothing is happening
    STATE_STARTING = 1  # sprint scheduled
    STATE_RUNNING = 2  # sprint is underway
    STATE_WAITING = 3  # sprint is over, waiting for final wordcounts

    # cap the sprint time minutes 'cos ppl are crazy
    MAX_SPRINT_TIME = 60
    # max start time
    MAX_START_TIME = 15
    # wait time, in minutes, for people to get thier word counts in
    # after a sprint runs
    WAIT_TIME = 3
    # the bot command prefix
    PREFIX = '!'

    # get the nick from the factory into our namespace
    def _get_nickname(self):
        return self.factory.nickname

    nickname = property(_get_nickname)

    def signedOn(self):
        """Callback for when we sign on to the network"""
        self.join(self.factory.channel)
        print( "Signed on as {}.".format(self.nickname))
        self.task = None
        self.notify_users = []
        self.reset_sprint()

    def joined(self, channel):
        """Callback for when we joined the channel"""
        print( "Joined %s." % channel )
        self.channel = channel

    def reset_sprint(self):
        """reset the sprint vars to zero
        """
        if self.task is not None:
            self.task.stop()
        self.current_state = self.STATE_NONE
        self.sprint_start = 0
        self.sprint_duration = 0
        self.sprint_duration_mins = 0
        self.sprint_wait = 0
        self.sprint_users = {}
        # oops, thx ccaatt
        #self.notify_users = []
        self.wait_users = []
        self.sprint_started_by = None
        self.task = None


    def privmsg(self, user, channel, msg):
        """
        Twisted callback for message, wrapped in a try
        """
        try:
            self.handle_privmsg(user, channel, msg)
        except Exception, e:
            self.msg(channel, 'umm, ok, something crazy just happened and I had to reset, sorry ;-;')
            self.reset_sprint()
            logging.info('[%s %s %s]' % (user, channel, msg))
            logging.exception(e)


    def handle_privmsg(self, user, channel, msg):
        """Callback for handling messages on the channel
        """
        # private message (whisper)
        if channel == self.nickname:
            u = user.split('!')[0]
            if u == OWNER_NICK:
                cmd = msg.split(' ')
                if len(cmd) > 1:
                    if cmd[0] == 'say':
                        self.msg(self.channel, ' '.join(cmd[1:]))
                    elif cmd[0] == 'me':
                        self.describe(self.channel, ' '.join(cmd[1:]))
            else:
                self.msg(u, 'hi, i am ' + self.nickname)

        # respond if we see our own NICK
        if self.nickname in msg:
            s = "♪ ♪ ♪ WUB WUB WUB ♪ ♪ ♪"
            resp = ['i am bot', 'lol', '¯\_(ツ)_/¯', 'haha', 'o_O', ':D', 'ummm, k', '(-_-)', s]
            self.say(self.channel, resp[random.randint(0, len(resp) - 1)])

        # commands
        if msg[0] == self.PREFIX:
            msg = msg[len(self.PREFIX):]

            # marvin
            if msg.startswith('lol'):
                marvin = ['i think you ought to know i\'m feeling very depressed',
                          'life? don\'t talk to me about life',
                          'you can blame the Sirius Cybernetics Corporation for making bots with GPP',
                          'incredible... it\'s even worse than i thought it could be',
                          'i\'d make a suggestion but you wouldn\'t listen',
                          'i\'ve been talking to the main computer... it hates me',
                          'not that anyone cares but the restaurant is at the other end of the universe',
                          'this will all end in tears',
                          'do you want me to sit in a corner and rust or just fall apart where i\'m standing',
                          'life. loathe it or ignore it. you can\'t like it'
                ]
                self.msg(channel, marvin[random.randint(0, len(marvin) - 1)])

            # choose opt1, opt2, opt3...
            elif msg.startswith('choose'):
                options = msg[7:].split(',')
                if len(options) == 1:
                    self.msg(channel, 'idk, try separating choices with a comma')
                    return
                self.msg(channel, '-> ' + options[random.randint(0, len(options) - 1)] + ' <- yep!')

            # give advice
            elif msg.startswith('8ball'):
                ans = ['It is certain', 'It is decidedly so', 'Without a doubt',
                       'Yes, definitely', 'You can rely on it', 'As I see it, yes',
                       'Most likely', 'Outlook good', 'Yes', 'Signs point to yes',
                       'Reply hazy, try again', 'Ask again later', 'Better not telll you now',
                       'Cannot predict now', 'Concentrate and ask again',
                       'Don\'t count on it', 'My reply is no', 'My sources say no',
                       'Outlook not so good', 'Very doubtful']
                self.msg(channel, ans[random.randint(0, len(ans) - 1)])

            # wrimo word count
            # TODO fix this cos it is nano specific and will make no sense
            # if it's run outside of november
            elif msg.startswith('goal') or msg.startswith('target'):
                d2 = date.today()
                d1 = date(d2.year, 10, 31)
                dys = (d2 - d1).days
                self.msg(channel, 'umm, today is day %d, goal of %d words' % (dys, (dys * 1667)))

            # ====================================
            # sprint commands     
            # ====================================
            # start a sprint or get status
            elif msg.startswith('sprint'):
                if self.current_state != self.STATE_NONE:
                    self.showstatus(channel)
                else:
                    self.startsprint(user, channel, msg)

            # status
            elif msg.startswith('status') or msg.startswith('time') or msg.startswith('sup'):
                self.showstatus(channel)

            # notify
            elif msg.startswith('notify') or msg.startswith('pingme'):
                u = user.split('!')
                if u[0] not in self.notify_users:
                    self.notify_users.append(u[0])
                    self.msg(channel, 'i will let you know!')
                else:
                    self.msg(channel, 'i got you ' + u[0])

            # withdraw   
            elif msg.startswith('withdraw'):
                u = user.split('!')
                if u[0] in self.sprint_users:
                    del self.sprint_users[u[0]]
                    self.msg(channel, '%s is out' % (u[0]))
                    if len(self.sprint_users) == 0:
                        self.reset_sprint()
                        self.msg(channel, 'everyone bailed, sprint aborted.')

            # join the sprint
            elif msg.startswith('join'):
                u = user.split('!')
                cmd = msg.split(' ')
                if u[0] in self.sprint_users:
                    self.msg(channel, 'you are already in %s' % u[0])
                    return
                base = 0
                if len(cmd) > 1 and self._is_int(cmd[1]):
                    base = int(cmd[1])
                # value is a list with [current,base]
                self.sprint_users[u[0]] = [0, base]
                self.msg(channel, '%s joined the sprint (base wc: %d)' % (u[0], base))

            # abort the sprint if one is running
            elif msg.startswith('abort'):
                u = user.split('!')
                if self.current_state != self.STATE_NONE:
                    if u[0] == self.sprint_started_by or u[0] == OWNER_NICK:
                        self.reset_sprint()
                        self.msg(channel, 'sprint aborted!')

            # final
            elif msg.startswith('finalize'):
                u = user.split('!')[0]
                if self.current_state == self.STATE_WAITING and (u == self.sprint_started_by or u == OWNER_NICK):
                    self.sprint_wait = 5

            # word count
            elif msg.startswith('wc'):
                u = user.split('!')
                if self.current_state == self.STATE_NONE:
                    self.showstatus(channel)
                    return
                cmd = msg.split(' ')
                if u[0] not in self.sprint_users:
                    self.msg(channel, 'type \'!join\' to join %s' % u[0])
                    return
                if len(cmd) == 1:
                    total = self.sprint_users[u[0]][0] - self.sprint_users[u[0]][1]
                    self.msg(channel, '%s %d words (%d/%d)' % (
                        u[0], total, self.sprint_users[u[0]][0], self.sprint_users[u[0]][1]))
                    return
                if len(cmd) > 1:
                    if cmd[1] == 'set':
                        if self._is_int(cmd[2]):
                            self.sprint_users[u[0]][1] = int(cmd[2])
                            self.msg(channel, '%s set wc at %s' % (u[0], cmd[2]))
                    elif cmd[1] == 'reset':
                        self.sprint_users[u[0]][1] = 0
                        self.msg(channel, '%s reset wc to 0' % (u[0]))

                    if len(cmd) == 2 and self._is_int(cmd[1]):
                        self.sprint_users[u[0]][0] = int(cmd[1])
                        cnt = self.sprint_users[u[0]][0] - self.sprint_users[u[0]][1]
                        self.msg(channel, '%s for %d words' % (u[0], cnt))
                        # we keep track of who has wc'd so we can end early if
                        # everyone ante's up
                        if self.current_state == self.STATE_WAITING:
                            if u[0] not in self.wait_users:
                                self.wait_users.append(u[0])

                                # ====================================
                                # /sprint commands
                                # ====================================

    def showstatus(self, channel):
        """Shows the status on the channel of the state machine"""
        if self.current_state == self.STATE_NONE:
            self.msg(channel, 'nothing is happening. my bot life is a void.')
            self.msg(channel, 'you can start something with \'!sprint start duration\'')
            self.msg(channel, '... or not, it\'s up to you really.')
        elif self.current_state == self.STATE_STARTING:
            self.msg(channel, 'sprint starts in %s and runs for %d minutes' % (
                self.minsec(self.sprint_start), self.sprint_duration_mins))
        elif self.current_state == self.STATE_RUNNING:
            self.msg(channel, 'sprint is running, %s left' % (self.minsec(self.sprint_duration)))
            # self.msg(channel, 'Participants: %s' % (','.join(self.sprint_users.keys())))
        elif self.current_state == self.STATE_WAITING:
            self.msg(channel, 'sprint is over, waiting for the word counts (%s)' % (self.minsec(self.sprint_wait)))
            s = [x for x in self.sprint_users.keys() if x not in self.wait_users]
            self.msg(channel, 'waiting for: ' + ' '.join(s))

    def startsprint(self, user, channel, msg):
        """Starts a new sprint"""
        cmd = msg.split(' ')
        if len(cmd) != 3:
            self.msg(channel, 'command is \'!sprint start duration\'')
            return
        if not self._is_int(cmd[1]) or not self._is_int(cmd[2]):
            self.msg(channel, 'umm, command is \'!sprint start duration\'')
            return
        if int(cmd[1]) <= 0 or int(cmd[2]) <= 0:
            self.msg(channel, 'let\'s try to keep things positive, command is \'!sprint start duration\'')
            return
        if int(cmd[1]) > self.MAX_START_TIME:
            self.msg(channel, 'i don\'t want to wait that long, try %d minutes or less' % self.MAX_START_TIME)
            return
        if int(cmd[2]) > self.MAX_SPRINT_TIME:
            self.msg(channel, 'no sprint for you! max sprint time is %d minutes' % self.MAX_SPRINT_TIME)
            return
        u = user.split('!')
        self.sprint_started_by = u[0]
        self.sprint_users[u[0]] = [0, 0]
        self.sprint_start = int(cmd[1]) * 60
        self.sprint_duration = int(cmd[2]) * 60
        self.sprint_duration_mins = int(cmd[2])
        self.msg(channel,
                 'sprint starts in %s minute(s) and runs for %s minute(s)' % (self.minsec(self.sprint_start), cmd[2]))
        self.msg(channel, 'type \'!join\' to get in on the action')
        # notify
        if len(self.notify_users) > 0:
            self.msg(channel, 'heads up!!: ' + ' '.join(self.notify_users))
            self.notify_users = []
        self.current_state = self.STATE_STARTING
        self.task = task.LoopingCall(self.tick)
        self.task.start(1.0)

    # one second tick
    def tick(self):
        """state machine, called every second once a sprint is started"""
        if self.current_state == self.STATE_STARTING:
            self.sprint_start -= 1
            if self.sprint_start == 30:
                self.msg(self.channel, 'sprint starts in %d seconds and runs for %d minutes' % (
                    self.sprint_start, self.sprint_duration_mins))
                self.msg(self.channel, 'type \'!join\' to get in on the action')
                self.msg(self.channel, 'participants: %s' % (' '.join(self.sprint_users.keys())))
            if self.sprint_start == 0:
                self.current_state = self.STATE_RUNNING
                self.msg(self.channel, 'Sprint started! Get busy!!!!')
                self.msg(self.channel, 'participants: %s' % (' '.join(self.sprint_users.keys())))
            return

        if self.current_state == self.STATE_RUNNING:
            self.sprint_duration -= 1
            if self.sprint_duration == 30:
                self.msg(self.channel, 'sprint ends in %d seconds' % self.sprint_duration)
                self.msg(self.channel, 'participants: %s' % (' '.join(self.sprint_users.keys())))
            if self.sprint_duration == 0:
                self.msg(self.channel,
                         'sprint is over. you have %d minute(s) send me your counts with \'!wc #\'' % self.WAIT_TIME)
                self.msg(self.channel, 'participants: %s' % (' '.join(self.sprint_users.keys())))
                self.current_state = self.STATE_WAITING
                self.sprint_wait = self.WAIT_TIME * 60
            return

        if self.current_state == self.STATE_WAITING:
            self.sprint_wait -= 1
            # if everyone has wc'd after we start waiting then we end early
            s = [c for c in self.sprint_users.keys() if c not in self.wait_users]
            if len(s) == 0:
                self.sprint_wait = 0
            if self.sprint_wait == 0:
                self.current_state = self.STATE_NONE
                self.task.stop()
                self.task = None
                self.announce_winner()
                self.reset_sprint()
            return

    # figure out who won
    def announce_winner(self):
        """Figures and shows the sprint winner and resets the users"""
        if len(self.sprint_users) == 0:
            # this should never happen, but hey
            return
        srted = sorted(self.sprint_users.items(), key=lambda x: (x[1][0] - x[1][1]), reverse=True)
        score = srted[0][1][0] - srted[0][1][1]
        self.msg(self.channel, '°o.O] %s [O.o° won the sprint with %d words (%d wpm)' %
                 (srted[0][0], score, (score / self.sprint_duration_mins)))
        total = sum(i - j for i, j in self.sprint_users.values())
        logging.info('sprint total %d' % total)
        self.sprint_users = {}
        self.wait_users = []

    # test if a string input is an int
    def _is_int(self, str):
        """Returns true if a string can be coerced into and integer"""
        try:
            int(str)
            return True
        except:
            return False

    # format some seconds
    def minsec(self, sec):
        """Given integer seconds, returns a string formatted and mins:seconds"""
        return '%d:%02d' % (sec / 60, sec % 60)


# twisted keeps spawning these i think, even if they are broken?
class LeBotFactory(protocol.ClientFactory):
    """Handle the sprintbot protocol"""
    protocol = SprintBot

    def __init__(self, channel, nickname=NICK):
        self.channel = channel
        self.nickname = nickname

    def clientConnectionLost(self, connector, reason):
        print("Lost connection (%s), reconnecting." % reason)
        connector.connect()

    def clientConnectionFailed(self, connector, reason):
        print("Could not connect: %s" % reason)

# =======
# M A I N
# =======
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--host', help='host name to connect to')
    parser.add_argument('-p', '--port', type=int, help='host port to connect to')
    parser.add_argument('-c', '--channel', help='host channel to connect to')
    parser.add_argument('-n', '--nick', help='the bot nick')
    parser.add_argument('-o', '--owner', help='the owner nick')
    args = parser.parse_args()

    if args.host is not None:
        SERVER = args.host
    if args.port is not None:
        SERVER_PORT = args.port
    if args.channel is not None:
        CHANNEL = args.channel
    if args.nick is not None:
        NICK = args.nick
    if args.owner is not None:
        OWNER_NICK = args.owner

    logging.basicConfig(format='%(asctime)s %(message)s', filename=NICK + '.log', level=logging.INFO)

    print('server: %s:%d channel: %s nick: %s owner: %s'
          % (SERVER, SERVER_PORT, CHANNEL, NICK, OWNER_NICK))

    reactor.connectTCP(SERVER, SERVER_PORT, LeBotFactory(CHANNEL))
    reactor.run()
